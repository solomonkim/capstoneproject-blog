<ul class="nav navbar-nav">
    <li>
        <a href="${pageContext.request.contextPath}/about">About</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/classes">Classes</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/contact">Contact</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/disclaimer">Disclaimer</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/admin">Admin</a>
    </li>
</ul>