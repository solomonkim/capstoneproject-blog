/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.blacksmithblogcapstone.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author apprentice
 */
public class BlogSummary {

    int blogId;
    String title;
    String author;
    int numComments;
    Date date;
    String category;
    String approvalStatus;

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
    

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCategory(String category) {
        this.category = category;
    }
   

    public BlogSummary(int blogId, String title, String author, int numComments, Timestamp date, String category, String approvalStatus) {
        this.blogId = blogId;
        this.title = title;
        this.author = author;
        this.numComments = numComments;
        this.date = date;
        this.category = category;
        this.approvalStatus = approvalStatus;
    }

    public BlogSummary() {
    }

    public int getBlogId() {
        return blogId;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getNumComments() {
        return numComments;
    }

    public Date getDate() {
        return date;
    }

    public String getCategory() {
        return category;
    }
    
}
