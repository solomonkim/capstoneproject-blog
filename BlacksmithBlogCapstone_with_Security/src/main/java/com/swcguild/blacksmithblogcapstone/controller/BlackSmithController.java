/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.blacksmithblogcapstone.controller;

import com.swcguild.blacksmithblogcapstone.dao.BlackSmithDao;

import com.swcguild.blacksmithblogcapstone.dto.BlogEntry;
import com.swcguild.blacksmithblogcapstone.dto.BlogSummary;
import com.swcguild.blacksmithblogcapstone.dto.Category;
import com.swcguild.blacksmithblogcapstone.dto.Comment;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class BlackSmithController {

    BlackSmithDao dao;

    @Inject
    public BlackSmithController(BlackSmithDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/archives", method = RequestMethod.GET)
    public String displayArchivesPage() {
        return "archives";
    }

    //Andrew Adding endpoints for static jsp's
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String displayAboutPage() {
        return "about";
    }

    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public String displayClassesPage() {
        return "classes";
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String displayContactPage() {
        return "contact";
    }

    @RequestMapping(value = "/disclaimer", method = RequestMethod.GET)
    public String displayDisclaimerPage() {
        return "disclaimer";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String displayAdminPage() {
        return "admin";
    }

    @RequestMapping(value = "/blogEntry/{title}", method = RequestMethod.GET)
//    @ResponseBody
    public String getBlogEntryByTitle(@PathVariable("title") String title, Model model) {
        BlogEntry blogEntry = dao.getBlogEntryByTitle(title);
        model.addAttribute("blogFound", blogEntry);
        return "fullBlog";
    }

    @RequestMapping(value = "/blogEntry", method = RequestMethod.GET)
    @ResponseBody
    public List<BlogEntry> getAllBlogEntries() {
        return dao.getAllBlogEntries();
    }

    @RequestMapping(value = "/blogId/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BlogEntry getBlogEntryById(@PathVariable("id") int id) {
        return dao.getBlogEntryById(id);
    }

    @RequestMapping(value = "/comment/{blogId}")
    @ResponseBody
    public List<Comment> getCommentsByBlogId(@PathVariable("blogId") int blogId) {
        return dao.getCommentsByBlogId(blogId);
    }

    @RequestMapping(value = "/blogSummary")
    @ResponseBody
    public List<BlogSummary> getBlogSummaries() {
        return dao.getBlogSummaries();
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> getAllCategories() {
        return dao.getAllCategories();
    }

    @Valid
    @RequestMapping(value = "/blogEntry", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public BlogEntry addBlogEntry(@Valid @RequestBody BlogEntry blogEntry) {
        return dao.addBlogEntry(blogEntry);
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Comment addComment(@RequestBody Comment comment) {
        return dao.addComment(comment);
    }

    @RequestMapping(value = "blogEntry/delete/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeBlogEntry(@PathVariable("id") int id) {

        List<Comment> commentsToDelete = dao.getCommentsByBlogId(id);
        for (Comment c : commentsToDelete) {
            dao.removeComment(c.getId());
        }
        dao.removeBlogEntry(id);

    }

    @RequestMapping(value = "comment/delete/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeComment(@PathVariable("id") int id) {
        dao.removeComment(id);
    }

    @RequestMapping(value = "edit/blogEntry/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void editBlogEntry(@PathVariable("id") int idOfBlogEntryToEdit, @RequestBody BlogEntry blogEntryEdits) {
        blogEntryEdits.setId(idOfBlogEntryToEdit);
        dao.editBlogEntry(blogEntryEdits);
    }

    @RequestMapping(value = "search/{searchTerm}", method = RequestMethod.GET)
    @ResponseBody
    public List<BlogEntry> searchBlogEntries(@PathVariable("searchTerm") String searchTerm) {
        System.out.println("Jesus from the fetus.");
        return dao.searchBlogEntries(searchTerm);
    }

    @RequestMapping(value = "search/category/{searchCategory}", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> searchBlogCategory(@PathVariable("searchCategory") String searchCategory) {
        return dao.searchCategory(searchCategory);
    }

    @RequestMapping(value = "approve/{blogEntryId}", method = RequestMethod.POST)
    @ResponseBody
    public void approveBlogEntry(@PathVariable("blogEntryId") int blogEntryId) {
        dao.approveBlogEntry(blogEntryId);
    }

    @RequestMapping(value = "blogEntry/category/{category}", method = RequestMethod.GET)
    @ResponseBody
    public List<BlogEntry> getBlogEntriesByCategory(@PathVariable("category") String searchCategory) {
        Predicate<BlogEntry> searchCatPred = (blog) -> blog.getCategory().toLowerCase().equals(searchCategory.toLowerCase());

        return dao.getAllBlogEntries().stream()
                .filter(searchCatPred)
                .collect(Collectors.toList());
    }

}
