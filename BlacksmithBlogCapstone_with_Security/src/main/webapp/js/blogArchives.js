$(document).ready(function () {
    //loadBlogs();
    loadSummaries();
    loadPopularPosts();
    
    
    
    $('#global-search-button').click(function (event) {

        $.ajax({
            type: 'GET',
            url: '/BlacksmithBlogCapstone/search/' + $('#global-search-term').val(),
            data: JSON.stringify({
                searchTerm: $('#global-search-term').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            //$('#global-search-term').val('');                        
            fillBlogEntries(data, status);

        });
        
    });
        
});



function loadPopularPosts() {
    var pBody = $('#popularRows');
    pBody.empty();

    $.ajax({
        url: '/BlacksmithBlogCapstone/blogSummary',
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillPopularBlogs(data, status);
    });
}

function loadBlogs() {
    var cBody = $('#contentRows');
    cBody.empty();

    $.ajax({
        url: "/BlacksmithBlogCapstone/blogEntry",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogEntries(data, status);
    });
}
function loadSummaries() {
    var cBody = $('#summaryRows');
    cBody.empty();

    $.ajax({
        url: "/BlacksmithBlogCapstone/blogSummary",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogSummaries(data, status);
    });
}
function fillBlogComments(comments, status) {

    var cBody = $('#blogComments');
    cBody.empty();
    
    
    $.each(comments, function (index, val) {
        var date = new Date(val.timestamp);
        cBody.append($('<h6><span class="glyphicon glyphicon-comment"></span> Comment by '+ val.name + '</h6>'));
        cBody.append($('<h6><span class="glyphicon glyphicon-time"></span>'+ ' '+  date + '</h6>'));
        cBody.append($('<p>' + val.body + '</p>'));        
        cBody.append($('<hr>'));
    });

}

function fillBlogSummaries(blogList, status) {

    var cBody = $('#summaryRows');
    cBody.empty();

    $.each(blogList, function (index, blogSummary) {
        var title = blogSummary.title;
        var new_title = encodeURIComponent(title.trim());
        cBody.append($('<tr>')
                .append($('<td align="left">').text(blogSummary.date))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                                })
                                .text(blogSummary.title)
                                )
                        )
                .append($('<td>').text(blogSummary.author))
                
                .append($('<td align="center">').text(blogSummary.numComments))
                
                );
    });
}

function fillPopularBlogs(blogList, status) {

    var dBody = $('#popularRows');
    dBody.empty();
    var count = 0;
    
    
    blogList.sort(function (a, b) {
        return b.numComments - a.numComments;
    });

    $.each(blogList, function (index, blogSummary) {
        var title = blogSummary.title;
        var new_title = encodeURIComponent(title.trim());
        dBody.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                                })
                                .text(blogSummary.title)
                                )
                        )
                .append($('<td>').text(blogSummary.numComments))

                );
    });
}

function deleteBlogEntry(id) {
    var cBody = $('#summaryRows');
    cBody.empty();
    var answer = confirm("Do you really want to delete this Blog Entry?");

    if (answer === true) {
        $.ajax({
            url: "/BlacksmithBlogCapstoneblogEntry/" + id,
            type: "DELETE",
            headers: {
                'Accept': 'application/json'
            }
        }).success(function (data, status) {
            fillBlogSummaries(data, status);
            loadSummaries();
        });
    }
}


function fillBlogEntries(blogList, status) {

    var cBody = $('#contentRows');
    cBody.empty();
    
    var blogSize = blogList.length;
    if (blogSize === 0)
        cBody.append($('<h3> No Blogs Found</h3>'));
    
    blogList.sort(function (a, b) {
        return b.timestamp - a.timestamp;
    });
        
    $.each(blogList, function (index, blogEntry) {
        var date = new Date(blogEntry.timestamp);
        cBody.append($('<h6><span class="glyphicon glyphicon-time"></span> Posted on '+ date.toString() +'&nbsp;&nbsp;' +'<span class="glyphicon glyphicon-comment"></span> <i> '+ blogEntry.authorName +'</i></h6>'));
        
        var title = blogEntry.title;
        var new_title = encodeURIComponent(title.trim());
        //new_title.replace('_',/%20/g);
                
        cBody.append($('<a>')
                .attr({                    
                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                })
                .html('<h3>'+blogEntry.title + '</h3>')
                );
        cBody.append($('<div class="row">' + '<div class="col-md-4">' +
                '<img class="img-responsive" src="img/rsz_blacksmithing.jpg" alt="">' + '</div>' + '<div class="col-md-8">' + blogEntry.body
                + '</div>'
                ));
        cBody.append($('<hr>'));
    });
}
