<ul class="nav navbar-nav">
    <li>
        <a href="${pageContext.request.contextPath}/about">About</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/classes">Classes</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/contact">Contact</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/disclaimer">Disclaimer</a>
    </li>
    <sec:authorize access="hasRole('ROLE_ANONYMOUS')" var="isAnon">
        <c:if test="${isAnon}">
            <li>
                <a href="${pageContext.request.contextPath}/login">Login</a>
            </li>
        </c:if>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')" var="isAdmin">
        <c:if test="${isAdmin}">
            <li>
                <a href="${pageContext.request.contextPath}/admin">Admin</a>
            </li>
        </c:if>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')" var="isAdmin">
        <c:if test="${isAdmin}">
            <li>
                <a href="${pageContext.request.contextPath}/logout">Logout</a>
            </li>
        </c:if>
    </sec:authorize>

</ul>